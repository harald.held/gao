from conans import ConanFile, CMake


class GaoConan(ConanFile):
    name = "gao"
    version = "0.0.2"
    license = "MIT"
    author = "Harald Held <harald.held@gmail.com>"
    url = "https://gitlab.com/harald.held/gao"
    description = "Simple MINLP solver via a genetic algorithm"
    topics = ("MINLP", "genetic algorithm")
    settings = "os", "compiler", "build_type", "arch"
    options = {"unit_tests": ["ON", "OFF"]}
    default_options = {"unit_tests": "OFF"}
    generators = "cmake"
    exports_sources = "*"

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions["BUILD_UNIT_TESTS"] = self.options.unit_tests
        cmake.configure()
        return cmake

    def requirements(self):
        if self.options.unit_tests == "ON":
            self.requires("catch2/2.13.6")

        self.requires("spdlog/1.8.5")
        self.requires("yaml-cpp/0.6.3")

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

        self.copy("gao-config.yml", src="src", dst="bin")

    def package_info(self):
        self.cpp_info.libs = ["gao"]
        self.cpp_info.cppflags = ["-fopenmp"]
        self.cpp_info.sharedlinkflags = ["-fopenmp"]
