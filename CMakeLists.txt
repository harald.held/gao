cmake_minimum_required(VERSION 3.10)

include(projectinfo.cmake)

project(${PROJECTNAME} VERSION ${PROJECTVERSION} LANGUAGES CXX)

# options ##############################################################################################################
option(BUILD_UNIT_TESTS "build unit tests" ON)

# setup Conan ##########################################################################################################
if (NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
    message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
    file(DOWNLOAD "https://raw.githubusercontent.com/conan-io/cmake-conan/master/conan.cmake"
         "${CMAKE_BINARY_DIR}/conan.cmake")
endif ()

include(${CMAKE_BINARY_DIR}/conan.cmake)

conan_add_remote(NAME conan-center URL https://center.conan.io)

conan_cmake_run(CONANFILE conanfile.py
                BASIC_SETUP CMAKE_TARGETS
                BUILD missing
                OPTIONS gao:unit_tests=${BUILD_UNIT_TESTS}
                )

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
########################################################################################################################

add_subdirectory(src)

if (BUILD_UNIT_TESTS)
    add_subdirectory(tests)
endif ()
