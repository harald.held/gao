#include "Config.h"
#include "logger.h"

#include <yaml-cpp/yaml.h>

#include <filesystem>

namespace fs = std::filesystem;

namespace gao
{

namespace
{
auto           lg             = logger();
constexpr auto configFileName = "gao-config.yml";
} // namespace

int Config::populationSize() const { return populationSize_; }

void Config::setPopulationSize(int value) { populationSize_ = value; }

double Config::mutationProbability() const { return mutationProbability_; }

void Config::setMutationProbability(double value) { mutationProbability_ = value; }

double Config::crossoverProbability() const { return crossoverProbability_; }

void Config::setCrossoverProbability(double value) { crossoverProbability_ = value; }

double Config::convergenceTolerance() const { return convergenceTolerance_; }

void Config::setConvergenceTolerance(double value) { convergenceTolerance_ = value; }

int Config::maxNumIterations() const { return maxNumIterations_; }

void Config::setMaxNumIterations(int maxNumIterations) { maxNumIterations_ = maxNumIterations; }

int Config::outputFrequency() const { return outputFrequency_; }

void Config::setOutputFrequency(int outputFrequency) { outputFrequency_ = outputFrequency; }

double Config::initialPenalizationFactor() const { return initialPenalizationFactor_; }

void Config::setInitialPenalizationFactor(double initialPenalizationFactor)
{
    if (initialPenalizationFactor > 0.)
    {
        initialPenalizationFactor_ = initialPenalizationFactor;
        return;
    }

    lg->warn("ignoring initial penalization factor of {} since it must be greater than 0", initialPenalizationFactor);
}

int Config::increasePenalFactorEveryNIterations() const { return increasePenalFactorEveryNIterations_; }

void Config::setIncreasePenalFactorEveryNIterations(int increasePenalFactorEveryNIterations)
{
    increasePenalFactorEveryNIterations_ = increasePenalFactorEveryNIterations;
}

double Config::additivePenalFactorIncrease() const { return additivePenalFactorIncrease_; }

void Config::setAdditivePenalFactorIncrease(double additivePenalFactorIncrease)
{
    additivePenalFactorIncrease_ = additivePenalFactorIncrease;
}

double Config::multiplicativePenalFactorIncrease() const { return multiplicativePenalFactorIncrease_; }

void Config::setMultiplicativePenalFactorIncrease(double multiplicativePenalFactorIncrease)
{
    if (multiplicativePenalFactorIncrease >= 1.)
    {
        multiplicativePenalFactorIncrease_ = multiplicativePenalFactorIncrease;
        return;
    }

    lg->warn("ignoring multiplicative penalization factor of {} since it must be greater than 1",
             multiplicativePenalFactorIncrease);
}

Config readConfigFromYml()
{
    Config config;

    if (!fs::exists(configFileName))
    {
        lg->warn("no config file 'gao-config.yml' found, using the default values");
        return config;
    }

    lg->info("loading options from config file '{}'", configFileName);

    YAML::Node configYml = YAML::LoadFile(configFileName);

    if (auto populationSizeNode = configYml["populationSize"]; populationSizeNode)
    {
        config.setPopulationSize(populationSizeNode.as<int>());
    }

    if (auto mutationProbabilityNode = configYml["mutationProbability"]; mutationProbabilityNode)
    {
        config.setMutationProbability(mutationProbabilityNode.as<double>());
    }

    if (auto crossoverProbabilityNode = configYml["crossoverProbability"]; crossoverProbabilityNode)
    {
        config.setCrossoverProbability(crossoverProbabilityNode.as<double>());
    }

    if (auto convergenceToleranceNode = configYml["convergenceTolerance"]; convergenceToleranceNode)
    {
        config.setConvergenceTolerance(convergenceToleranceNode.as<double>());
    }

    if (auto maxNumIterationsNode = configYml["maxNumIterations"]; maxNumIterationsNode)
    {
        config.setMaxNumIterations(maxNumIterationsNode.as<int>());
    }

    if (auto outputFrequencyNode = configYml["outputFrequency"]; outputFrequencyNode)
    {
        config.setOutputFrequency(outputFrequencyNode.as<int>());
    }

    if (auto initialPenalFactorNode = configYml["initialPenalizationFactor"]; initialPenalFactorNode)
    {
        config.setInitialPenalizationFactor(initialPenalFactorNode.as<double>());
    }

    if (auto increasePenalFactorEveryNIterNode = configYml["increasePenalFactorEveryNIterations"];
        increasePenalFactorEveryNIterNode)
    {
        config.setIncreasePenalFactorEveryNIterations(increasePenalFactorEveryNIterNode.as<int>());
    }

    if (auto additivePenalFactorIncreaseNode = configYml["additivePenalFactorIncrease"];
        additivePenalFactorIncreaseNode)
    {
        config.setAdditivePenalFactorIncrease(additivePenalFactorIncreaseNode.as<double>());
    }

    if (auto multiplicativePenalFactorIncreaseNode = configYml["multiplicativePenalFactorIncrease"];
        multiplicativePenalFactorIncreaseNode)
    {
        config.setMultiplicativePenalFactorIncrease(multiplicativePenalFactorIncreaseNode.as<double>());
    }

    return config;
}

} // namespace gao
