#include "Variable.h"
#include "logger.h"

#include <limits>
#include <utility>

namespace gao
{

auto lg = logger();

Variable::Variable(std::string name, Type type)
    : name_(std::move(name))
    , type_(type)
{
    switch (type_)
    {
    case Type::Continuous:
        // the factor 0.49 here will prevent overflow of std::numeric_limits when generating random numbers
        lb_ = 0.49 * std::numeric_limits<double>::lowest();
        ub_ = 0.49 * std::numeric_limits<double>::max();
        break;
    case Type::Integer:
        lb_ = std::numeric_limits<int>::lowest();
        ub_ = std::numeric_limits<int>::max();
        break;
    case Type::Binary:
        lb_ = 0.;
        ub_ = 1.;
        break;
    }
}

Variable::~Variable() = default;

double Variable::lb() const { return lb_; }

void Variable::setLb(double value)
{
    if (type_ == Type::Binary)
    {
        lg->warn("setting a binary variable's lower bound doesn't make sense, ignoring it");
        return;
    }

    lb_ = value;
}

double Variable::ub() const { return ub_; }

void Variable::setUb(double value)
{
    if (type_ == Type::Binary)
    {
        lg->warn("setting a binary variable's upper bound doesn't make sense, ignoring it");
        return;
    }

    ub_ = value;
}

std::string_view Variable::name() const { return name_; }

auto Variable::type() const -> Type { return type_; }

} // namespace gao
