#ifndef LOGGER_H
#define LOGGER_H

#include <memory>

#include <spdlog/logger.h>

namespace gao
{
std::shared_ptr<spdlog::logger> logger();
}

#endif // LOGGER_H
