#include "Problem.h"
#include "Variable.h"

#include <algorithm>
#include <iterator>
#include <utility>

namespace gao
{

Problem::Problem() = default;

size_t Problem::numVariables() const { return vars_.size(); }

Problem::~Problem() = default;

void Problem::addVariable(std::unique_ptr<Variable> var) { vars_.emplace_back(std::move(var)); }

std::vector<const Variable *> Problem::variables() const
{
    std::vector<const Variable *> vars;

    std::transform(std::cbegin(vars_), std::cend(vars_), std::back_inserter(vars),
                   [](const std::unique_ptr<Variable> &var) { return var.get(); });

    return vars;
}

void Problem::setObjective(std::function<double(const std::vector<VariableValue> &)> f) { obj_ = std::move(f); }

std::function<double(const std::vector<VariableValue> &)> Problem::obj() const { return obj_; }

void Problem::addInequalityConstraint(std::function<double(const std::vector<VariableValue> &)> g)
{
    inequalityConstraints_.emplace_back(std::move(g));
}

const std::vector<std::function<double(const std::vector<VariableValue> &)>> &Problem::inequalityConstraints() const
{
    return inequalityConstraints_;
}

void Problem::addEqualityConstraint(std::function<double(const std::vector<VariableValue> &)> h)
{
    equalityConstraints_.emplace_back(std::move(h));
}

const std::vector<std::function<double(const std::vector<VariableValue> &)>> &Problem::equalityConstraints() const
{
    return equalityConstraints_;
}

} // namespace gao
