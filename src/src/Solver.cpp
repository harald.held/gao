#include "Solver.h"
#include "Problem.h"
#include "logger.h"

#include <algorithm>
#include <iterator>
#include <random>
#include <set>

namespace gao
{

namespace
{
auto lg = logger();

void crossover(std::vector<VariableValue> &parent1, std::vector<VariableValue> &parent2)
{
    // cf. http://www.geatbx.com/docu/algindex-03.html
    const size_t numVars = parent1.size();
    assert(parent2.size() == numVars);

    const auto &originalParent1 = parent1;
    const auto &originalParent2 = parent2;

#pragma omp parallel for
    for (int v = 0; v < numVars; ++v)
    {
        static thread_local std::random_device seeder;
        static thread_local std::mt19937       rd(seeder());

        switch (originalParent1[v].variable->type())
        {
        case Variable::Type::Continuous: {
            std::uniform_real_distribution uniformDist(-0.25, std::nextafter(1.25, 2.));

            const auto alpha1 = uniformDist(rd);
            const auto alpha2 = uniformDist(rd);

            parent1[v].value =
                std::min(std::max(originalParent1[v].variable->lb(),
                                  alpha1 * originalParent1[v].value + (1. - alpha1) * originalParent2[v].value),
                         originalParent1[v].variable->ub());
            parent2[v].value =
                std::min(std::max(originalParent2[v].variable->lb(),
                                  alpha2 * originalParent2[v].value + (1. - alpha2) * originalParent1[v].value),
                         originalParent2[v].variable->ub());

            break;
        }
        case Variable::Type::Integer:
        case Variable::Type::Binary: {
            std::uniform_int_distribution uniformDist(0, 1);

            const auto alpha1 = uniformDist(rd);
            const auto alpha2 = uniformDist(rd);

            parent1[v].value =
                std::min(std::max(std::ceil(originalParent1[v].variable->lb()),
                                  alpha1 * originalParent1[v].value + (1. - alpha1) * originalParent2[v].value),
                         std::floor(originalParent1[v].variable->ub()));
            parent2[v].value =
                std::min(std::max(std::ceil(originalParent1[v].variable->lb()),
                                  alpha2 * originalParent1[v].value + (1. - alpha2) * originalParent2[v].value),
                         std::floor(originalParent1[v].variable->ub()));

            break;
        }
        }
    }
}

void mutate(std::vector<VariableValue> &offspring1, std::vector<VariableValue> &offspring2)
{
    // cf. http://www.geatbx.com/docu/algindex-04.html#P659_42386 (only for inspiration; this turned out very different)
    const size_t numVars = offspring1.size();

    assert(offspring2.size() == numVars);

#pragma omp parallel for
    for (int v = 0; v < numVars; ++v)
    {
        static thread_local std::random_device seeder;
        static thread_local std::mt19937       rd(seeder());

        switch (offspring1[v].variable->type())
        {
        case Variable::Type::Continuous: {
            std::normal_distribution<double> normalDist1(0., 0.5 * std::abs(offspring1[v].value));
            std::normal_distribution<double> normalDist2(0., 0.5 * std::abs(offspring2[v].value));

            offspring1[v].value += normalDist1(rd);
            offspring2[v].value += normalDist2(rd);

            offspring1[v].value =
                std::min(std::max(offspring1[v].variable->lb(), offspring1[v].value), offspring1[v].variable->ub());
            offspring2[v].value =
                std::min(std::max(offspring2[v].variable->lb(), offspring2[v].value), offspring2[v].variable->ub());

            break;
        }
        case Variable::Type::Integer:
        case Variable::Type::Binary: {
            std::uniform_int_distribution<int> uniformDist(0, 31);

            const int numPossibleBitsChange = uniformDist(rd);

            std::set<int> changedBits;

            for (int b = 0; b < numPossibleBitsChange; ++b)
            {
                if (changedBits.count(b) > 0)
                {
                    continue;
                }

                offspring1[v].value = static_cast<int>(offspring1[v].value) ^ (1 << uniformDist(rd));
                offspring2[v].value = static_cast<int>(offspring2[v].value) ^ (1 << uniformDist(rd));

                offspring1[v].value = std::min(std::max(std::ceil(offspring1[v].variable->lb()), offspring1[v].value),
                                               std::floor(offspring1[v].variable->ub()));
                offspring2[v].value = std::min(std::max(std::ceil(offspring2[v].variable->lb()), offspring2[v].value),
                                               std::floor(offspring2[v].variable->ub()));

                changedBits.insert(b);
            }

            break;
        }
        }
    }
}

std::function<double(const std::vector<VariableValue> &)> penalizationFunction(const Problem &problem,
                                                                               double         penalizationFactor)
{
    const auto &inequalityConstraints = problem.inequalityConstraints();
    const auto &equalityConstraints   = problem.equalityConstraints();
    auto  obj                   = problem.obj();

    if (inequalityConstraints.empty())
    {
        return obj;
    }

    return [=](const std::vector<VariableValue> &x) {
        const size_t numIneqConstraints = inequalityConstraints.size();
        const size_t numEqConstraints   = equalityConstraints.size();
        double       penalization       = 0.;

#pragma omp parallel for reduction(+ : penalization)
        for (int i = 0; i < numIneqConstraints; ++i)
        {
            penalization += std::pow(std::max(0., inequalityConstraints[i](x)), 2.);
        }

#pragma omp parallel for reduction(+ : penalization)
        for (int i = 0; i < numEqConstraints; ++i)
        {
            penalization += std::pow(equalityConstraints[i](x), 2.);
        }

        return obj(x) - penalizationFactor * penalization;
    };
}
} // namespace

Solver::Solver(const Problem &problem, std::optional<Config> config)
    : problem_(problem)
{
    if (!config)
    {
        config_ = readConfigFromYml();
    }
    else
    {
        config_ = *config;
    }

    initPopulation();
}

size_t Solver::populationSize() const { return population_.size(); }

const std::vector<std::vector<VariableValue>> &Solver::population() const { return population_; }

void Solver::solve()
{
    if (!problem_.obj())
    {
        lg->error("no objective function specified; the population's fitness values cannot be computed");
        return;
    }

    static thread_local std::random_device seeder;
    static thread_local std::mt19937       rd(seeder());
    std::uniform_real_distribution         uniformDist(0., std::nextafter(1., 2.));

    int iteration          = 0;
    int convergenceCounter = 0;

    double penalFactor = config_.initialPenalizationFactor();

    while (iteration++ < config_.maxNumIterations())
    {
        const auto fitnessFunc = penalizationFunction(problem_, penalFactor);

        if (problem_.hasConstraints() && penalFactor < 1.E10 &&
            iteration % config_.increasePenalFactorEveryNIterations() == 0)
        {
            penalFactor *= config_.multiplicativePenalFactorIncrease();
            penalFactor += config_.additivePenalFactorIncrease();
        }

        auto fitnessValues                  = evaluateFitnessValues(fitnessFunc);
        const auto [minFitness, maxFitness] = std::minmax_element(fitnessValues.cbegin(), fitnessValues.cend());

        if (iteration % config_.outputFrequency() == 0)
        {
            lg->info("generation {}: {} <= fitness <= {}", iteration, *minFitness, *maxFitness);
        }

        if (*maxFitness - *minFitness <= config_.convergenceTolerance())
        {
            if (convergenceCounter++ >= 10)
            {
                break;
            }

            lg->info("(iteration {}) stop if no change in {} iterations", iteration, 10 - convergenceCounter);
        }
        else
        {
            convergenceCounter = 0;
        }

        const auto parentIndices = selectParentIndices(fitnessValues);

        auto offspring1 = population_[parentIndices[0]];
        auto offspring2 = population_[parentIndices[1]];

        const auto crossoverProb = uniformDist(rd);
        bool       crossoverDone = false;

        if (crossoverProb <= config_.crossoverProbability())
        {
            lg->debug("performing crossover in iteration {}", iteration);
            crossover(offspring1, offspring2);
            crossoverDone = true;
        }

        const auto mutationProb = uniformDist(rd);
        bool       mutationDone = false;

        if (mutationProb <= config_.mutationProbability())
        {
            lg->debug("performing mutation in iteration {}", iteration);
            mutate(offspring1, offspring2);
            mutationDone = true;
        }

        // avoid no change whatsoever in the population
        if (!crossoverDone && !mutationDone)
        {
            lg->debug("performing mutation in iteration {} to avoid no change", iteration);
            mutate(offspring1, offspring2);
        }

        // reinsertion
        const auto fitnessOffspring1 = fitnessFunc(offspring1);
        const auto fitnessOffspring2 = fitnessFunc(offspring2);

        lg->debug("fitness offspring1 = {}\tfitness offspring2 = {}", fitnessOffspring1, fitnessOffspring2);

        std::vector<size_t> indicesWorseFitness1;
        std::vector<size_t> indicesWorseFitness2;

        for (size_t fv = 0, len = fitnessValues.size(); fv < len; ++fv)
        {
            if (fitnessOffspring1 > fitnessValues[fv])
            {
                indicesWorseFitness1.push_back(fv);
            }

            if (fitnessOffspring2 > fitnessValues[fv])
            {
                indicesWorseFitness2.push_back(fv);
            }
        }

        std::sort(indicesWorseFitness1.begin(), indicesWorseFitness1.end(),
                  [&fitnessValues](size_t i1, size_t i2) { return fitnessValues[i1] < fitnessValues[i2]; });

        if (!indicesWorseFitness1.empty())
        {
            const size_t idx   = indicesWorseFitness1[0];
            population_[idx]   = offspring1;
            fitnessValues[idx] = fitnessOffspring1;
        }

        std::sort(indicesWorseFitness2.begin(), indicesWorseFitness2.end(),
                  [&fitnessValues](size_t i1, size_t i2) { return fitnessValues[i1] < fitnessValues[i2]; });

        if (!indicesWorseFitness2.empty())
        {
            const size_t idx = indicesWorseFitness2[0];
            population_[idx] = offspring2;
        }
    }

    lg->info("finished optimization after {} iterations", iteration);
}

std::pair<std::vector<VariableValue>, double> Solver::bestSolutionAndObjectiveValue() const
{
    const auto fitnessValues = evaluateFitnessValues(problem_.obj());

    auto         it  = std::max_element(fitnessValues.cbegin(), fitnessValues.cend());
    const size_t idx = std::distance(fitnessValues.cbegin(), it);

    return std::make_pair(population_[idx], fitnessValues[idx]);
}

void Solver::initPopulation()
{
    const auto vars           = problem_.variables();
    const auto numVars        = problem_.numVariables();
    const int  populationSize = config_.populationSize();

    lg->info("start initializing a population of size {}", populationSize);

    population_.resize(populationSize);

#pragma omp parallel for
    for (int i = 0; i < populationSize; ++i)
    {
        auto &currentIndividual = population_[i];

        static thread_local std::random_device seeder;
        static thread_local std::mt19937       rd(seeder());

        currentIndividual.resize(numVars);

        for (size_t j = 0; j < numVars; ++j)
        {
            const auto *var = vars[j];

            std::function<double(void)> generateRandomVarValue;

            switch (var->type())
            {
            case Variable::Type::Continuous:
                generateRandomVarValue = [var]() {
                    std::uniform_real_distribution<double> uniformRealDist(
                        var->lb(), std::nextafter(var->ub(), 0.49 * std::numeric_limits<double>::max()));

                    return uniformRealDist(rd);
                };
                break;
            case Variable::Type::Integer:
            case Variable::Type::Binary:
                generateRandomVarValue = [var]() {
                    std::uniform_int_distribution<int> uniformIntDist(std::ceil(var->lb()), std::floor(var->ub()));

                    return uniformIntDist(rd);
                };
                break;
            }

            currentIndividual[j].variable = var;
            currentIndividual[j].value    = generateRandomVarValue();
        }
    }

    lg->info("done initializing a population of size {}", populationSize);
}

std::vector<double> Solver::evaluateFitnessValues(
    const std::function<double(const std::vector<VariableValue> &)> &fitnessFunc) const
{
    const auto populationSize = config_.populationSize();

    std::vector<double> fitnessValues(populationSize, 0.);

    lg->debug("start evaluating the population's fitness values");

#pragma omp parallel for
    for (int i = 0; i < populationSize; ++i)
    {
        fitnessValues[i] = fitnessFunc(population_[i]);
    }

    lg->debug("done evaluating the population's fitness values");

    return fitnessValues;
}

std::vector<size_t> Solver::selectParentIndices(const std::vector<double> &fitnessValues) const
{
    std::set<int> parentsUnique;
    const auto    k = static_cast<size_t>(std::max(2., std::ceil(0.2 * population_.size())));

    lg->debug("selecting the best 2 parents out of a random selection of {}", k);

    static thread_local std::random_device seeder;
    static thread_local std::mt19937       rd(seeder());
    std::uniform_int_distribution<size_t>  uniformIntDist(0, population_.size() - 1);

    while (parentsUnique.size() != k)
    {
        parentsUnique.insert(uniformIntDist(rd));
    }

    auto parentsSorted = std::vector<size_t>(parentsUnique.cbegin(), parentsUnique.cend());
    std::sort(parentsSorted.begin(), parentsSorted.end(),
              [&fitnessValues](size_t idx1, size_t idx2) { return fitnessValues[idx1] > fitnessValues[idx2]; });

    return std::vector<size_t>(parentsSorted.cbegin(), parentsSorted.cbegin() + 2);
}

} // namespace gao
