#include "logger.h"

#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

namespace gao
{

namespace
{
constexpr auto loggerName = "gao";
}

std::shared_ptr<spdlog::logger> logger()
{
    std::shared_ptr<spdlog::logger> logger;

    if ((logger = spdlog::get(loggerName)))
    {
        return logger;
    }

    logger = spdlog::stdout_color_mt(loggerName);
    logger->set_level(spdlog::level::info);

    return logger;
}

} // namespace gao
