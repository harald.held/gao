#ifndef PROBLEM_H
#define PROBLEM_H

#include <functional>
#include <memory>
#include <vector>

namespace gao
{

class Variable;
struct VariableValue;

class Problem
{
public:
    Problem();
    virtual ~Problem();

    [[nodiscard]] size_t numVariables() const;

    void                                        addVariable(std::unique_ptr<Variable> var);
    [[nodiscard]] std::vector<const Variable *> variables() const;

    /// this objective is always maximized
    void setObjective(std::function<double(const std::vector<VariableValue> &)>);
    [[nodiscard]] std::function<double(const std::vector<VariableValue> &)> obj() const;

    /**
     * @param g constraint function of the form g(x) <= 0
     */
    void addInequalityConstraint(std::function<double(const std::vector<VariableValue> &)> g);
    [[nodiscard]] const std::vector<std::function<double(const std::vector<VariableValue> &)>> &inequalityConstraints()
        const;

    /**
     * @param h constraint function of the form h(x) == 0
     */
    void addEqualityConstraint(std::function<double(const std::vector<VariableValue> &)> h);
    [[nodiscard]] const std::vector<std::function<double(const std::vector<VariableValue> &)>> &equalityConstraints()
        const;

    [[nodiscard]] bool hasConstraints() const
    {
        return !inequalityConstraints_.empty() || !equalityConstraints_.empty();
    }

private:
    std::vector<std::unique_ptr<Variable>>                    vars_;
    std::function<double(const std::vector<VariableValue> &)> obj_;

    std::vector<std::function<double(const std::vector<VariableValue> &)>> inequalityConstraints_;
    std::vector<std::function<double(const std::vector<VariableValue> &)>> equalityConstraints_;
};

} // namespace gao

#endif // PROBLEM_H
