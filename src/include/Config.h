#ifndef CONFIG_H
#define CONFIG_H

#include <string>

namespace gao
{

class Config
{
public:
    [[nodiscard]] int populationSize() const;
    void              setPopulationSize(int value);

    [[nodiscard]] double mutationProbability() const;
    void                 setMutationProbability(double value);

    [[nodiscard]] double crossoverProbability() const;
    void                 setCrossoverProbability(double value);

    [[nodiscard]] double convergenceTolerance() const;
    void                 setConvergenceTolerance(double value);

    [[nodiscard]] int maxNumIterations() const;
    void              setMaxNumIterations(int maxNumIterations);

    [[nodiscard]] int outputFrequency() const;
    void              setOutputFrequency(int outputFrequency);

    [[nodiscard]] double initialPenalizationFactor() const;
    void                 setInitialPenalizationFactor(double initialPenalizationFactor);

    [[nodiscard]] int increasePenalFactorEveryNIterations() const;
    void              setIncreasePenalFactorEveryNIterations(int increasePenalFactorEveryNIterations);

    [[nodiscard]] double additivePenalFactorIncrease() const;
    void                 setAdditivePenalFactorIncrease(double additivePenalFactorIncrease);

    [[nodiscard]] double multiplicativePenalFactorIncrease() const;
    void                 setMultiplicativePenalFactorIncrease(double multiplicativePenalFactorIncrease);

private:
    int    populationSize_                      = 100;
    double mutationProbability_                 = 0.02;
    double crossoverProbability_                = 0.8;
    double convergenceTolerance_                = 1.E-4;
    int    maxNumIterations_                    = 10000;
    int    outputFrequency_                     = 1;
    double initialPenalizationFactor_           = 0.01;
    int    increasePenalFactorEveryNIterations_ = 10;
    double additivePenalFactorIncrease_         = 0.;
    double multiplicativePenalFactorIncrease_   = 1.1;
};

Config readConfigFromYml();

} // namespace gao

#endif // CONFIG_H
