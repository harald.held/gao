#ifndef VARIABLE_H
#define VARIABLE_H

#include <string>

namespace gao
{

class Variable
{
public:
    enum class Type
    {
        Continuous,
        Integer,
        Binary
    };

    Variable(std::string name, Type type);
    ~Variable();

    [[nodiscard]] double lb() const;
    void                 setLb(double value);

    [[nodiscard]] double ub() const;
    void                 setUb(double value);

    [[nodiscard]] std::string_view name() const;

    [[nodiscard]] auto type() const -> Type;

private:
    std::string name_;
    Type        type_;
    double      lb_;
    double      ub_;
};

struct VariableValue
{
    const Variable *variable = nullptr;
    double          value    = 0.;
};

} // namespace gao

#endif // VARIABLE_H
