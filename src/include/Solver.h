#ifndef SOLVER_H
#define SOLVER_H

#include "Config.h"
#include "Variable.h"

#include <functional>
#include <optional>
#include <vector>

namespace gao
{

class Problem;

class Solver
{
public:
    explicit Solver(const Problem &problem, std::optional<Config> config = {});

    [[nodiscard]] size_t populationSize() const;

    [[nodiscard]] const std::vector<std::vector<VariableValue>> &population() const;

    void                                                        solve();
    [[nodiscard]] std::pair<std::vector<VariableValue>, double> bestSolutionAndObjectiveValue() const;

private:
    Config                                  config_;
    const Problem &                         problem_;
    std::vector<std::vector<VariableValue>> population_;

    void                initPopulation();
    std::vector<double> evaluateFitnessValues(
        const std::function<double(const std::vector<VariableValue> &)> &fitnessFunc) const;
    [[nodiscard]] std::vector<size_t> selectParentIndices(const std::vector<double> &fitnessValues) const;
};

} // namespace gao

#endif // SOLVER_H
