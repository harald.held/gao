#include <catch2/catch.hpp>

#include <Problem.h>
#include <Variable.h>

using namespace gao;

SCENARIO("a new Problem instance can be created", "[problem]")
{
    GIVEN("a new Problem instance")
    {
        Problem problem;

        WHEN("querying the number of variables")
        {
            THEN("it is 0") { REQUIRE(problem.numVariables() == 0); }
        }

        WHEN("adding a variable to the problem")
        {
            problem.addVariable(std::make_unique<Variable>("x", Variable::Type::Continuous));

            THEN("the problem has 1 variable") { REQUIRE(problem.numVariables() == 1); }

            THEN("the problem's variables consists of one variable")
            {
                const auto vars = problem.variables();

                REQUIRE(vars.size() == 1);

                AND_THEN("that one variable's name is 'x'") { REQUIRE(vars[0]->name() == "x"); }
            }
        }
    }
}
