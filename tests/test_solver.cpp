#include <catch2/catch.hpp>

#include <Config.h>
#include <Problem.h>
#include <Solver.h>

using namespace gao;

SCENARIO("a Solver object can be initialized for a given Problem instance", "[solver]")
{
    GIVEN("a Problem instance with 1 continuous, 1 integer, and 1 binary variable with default bounds")
    {
        Problem prob;

        prob.addVariable(std::make_unique<Variable>("x", Variable::Type::Continuous));
        prob.addVariable(std::make_unique<Variable>("y", Variable::Type::Integer));
        prob.addVariable(std::make_unique<Variable>("z", Variable::Type::Binary));

        WHEN("creating a Solver instance with that problem")
        {
            Solver solver(prob);

            THEN("the population size equals the corresponding configured population size")
            {
                const Config config = readConfigFromYml();

                REQUIRE(config.populationSize() == solver.populationSize());
            }
        }
    }

    GIVEN("a Problem instance with 1 continuous, 1 integer, and 1 binary variable with tighter bounds")
    {
        Problem prob;

        auto v1 = std::make_unique<Variable>("x", Variable::Type::Continuous);
        v1->setLb(0.1);
        v1->setUb(0.9);

        auto v2 = std::make_unique<Variable>("y", Variable::Type::Integer);
        v2->setLb(3.);
        v2->setUb(15.);

        auto v3 = std::make_unique<Variable>("z", Variable::Type::Binary);

        prob.addVariable(std::move(v1));
        prob.addVariable(std::move(v2));
        prob.addVariable(std::move(v3));

        WHEN("creating a Solver instance with that problem")
        {
            Solver solver(prob);

            THEN("the population size equals the corresponding configured population size")
            {
                const Config config = readConfigFromYml();

                REQUIRE(config.populationSize() == solver.populationSize());
            }

            THEN("all initialized values are within their specified ranges, and the order of variables is as specified "
                 "before")
            {
                const auto &population = solver.population();

                for (const auto &individual : population)
                {
                    REQUIRE(individual.size() == 3);

                    // check if the order of variables is preserved
                    REQUIRE(individual[0].variable->name() == "x");
                    REQUIRE(individual[1].variable->name() == "y");
                    REQUIRE(individual[2].variable->name() == "z");

                    // check bounds
                    REQUIRE(individual[0].value >= 0.1);
                    REQUIRE(individual[0].value <= 0.9);

                    REQUIRE(individual[1].value >= 3.);
                    REQUIRE(individual[1].value <= 15.);

                    REQUIRE((individual[2].value == 0. || individual[2].value == 1.));
                }
            }
        }
    }
}

SCENARIO("a well defined problem can be solved", "[solving]")
{
    GIVEN("a well defined optimization problem with only continuous variables")
    {
        // min x^2 + y^2 s.t. 0.1 <= x <= 0.9, 1.5 <= y <= 3.8
        // => expected optimum: (x, y) = (0.1, 1.5) with best objective value 2.26
        Problem problem;

        // setup vars
        {
            auto x = std::make_unique<Variable>("x", Variable::Type::Continuous);
            x->setLb(0.1);
            x->setUb(0.9);

            auto y = std::make_unique<Variable>("y", Variable::Type::Continuous);
            y->setLb(1.5);
            y->setUb(3.8);

            problem.addVariable(std::move(x));
            problem.addVariable(std::move(y));
        }

        problem.setObjective([](const std::vector<VariableValue> &vars) {
            REQUIRE(vars.size() == 2);

            return -(vars[0].value * vars[0].value + vars[1].value * vars[1].value);
        });

        AND_GIVEN("a solver for this problem")
        {
            Solver solver(problem);

            WHEN("solving the problem")
            {
                solver.solve();
                const auto [bestSolution, bestObjectiveValue] = solver.bestSolutionAndObjectiveValue();

                const auto bestX = bestSolution[0].value;
                const auto bestY = bestSolution[1].value;

                THEN("the best value for x is 0.1") { REQUIRE(bestX == Approx(0.1)); }
                THEN("the best value for y is 1.5") { REQUIRE(bestY == Approx(1.5)); }
                THEN("the best objective value is 2.26") { REQUIRE(bestObjectiveValue == Approx(-2.26)); }
            }
        }
    }

    GIVEN("a well defined optimization problem with only integer variables")
    {
        // min x^2 + y^2 s.t. 0.1 <= x <= 2.9, 1.5 <= y <= 3.8, x and y integer
        // => expected optimum: (x, y) = (1, 2) with best objective value 5
        Problem problem;

        // setup vars
        {
            auto x = std::make_unique<Variable>("x", Variable::Type::Integer);
            x->setLb(0.1);
            x->setUb(2.9);

            auto y = std::make_unique<Variable>("y", Variable::Type::Integer);
            y->setLb(1.5);
            y->setUb(3.8);

            problem.addVariable(std::move(x));
            problem.addVariable(std::move(y));
        }

        problem.setObjective([](const std::vector<VariableValue> &vars) {
            REQUIRE(vars.size() == 2);

            return -(vars[0].value * vars[0].value + vars[1].value * vars[1].value);
        });

        AND_GIVEN("a solver for this problem")
        {
            Solver solver(problem);

            WHEN("solving the problem")
            {
                solver.solve();
                const auto [bestSolution, bestObjectiveValue] = solver.bestSolutionAndObjectiveValue();

                const auto bestX = bestSolution[0].value;
                const auto bestY = bestSolution[1].value;

                THEN("the best value for x is 1") { REQUIRE(bestX == Approx(1.)); }
                THEN("the best value for y is 2") { REQUIRE(bestY == Approx(2.)); }
                THEN("the best objective value is 5") { REQUIRE(bestObjectiveValue == Approx(-5.)); }
            }
        }
    }

    GIVEN("a well defined optimization problem with one continuous and one integer variables")
    {
        // min x^2 + y^2 s.t. 0.1 <= x <= 2.9, 1.5 <= y <= 3.8, x integer
        // => expected optimum: (x, y) = (1, 1.5) with best objective value 3.25
        Problem problem;

        // setup vars
        {
            auto x = std::make_unique<Variable>("x", Variable::Type::Integer);
            x->setLb(0.1);
            x->setUb(2.9);

            auto y = std::make_unique<Variable>("y", Variable::Type::Continuous);
            y->setLb(1.5);
            y->setUb(3.8);

            problem.addVariable(std::move(x));
            problem.addVariable(std::move(y));
        }

        problem.setObjective([](const std::vector<VariableValue> &vars) {
            REQUIRE(vars.size() == 2);

            return -(vars[0].value * vars[0].value + vars[1].value * vars[1].value);
        });

        AND_GIVEN("a solver for this problem")
        {
            Solver solver(problem);

            WHEN("solving the problem")
            {
                solver.solve();
                const auto [bestSolution, bestObjectiveValue] = solver.bestSolutionAndObjectiveValue();

                const auto bestX = bestSolution[0].value;
                const auto bestY = bestSolution[1].value;

                THEN("the best value for x is 1") { REQUIRE(bestX == Approx(1.)); }
                THEN("the best value for y is 1.5") { REQUIRE(bestY == Approx(1.5)); }
                THEN("the best objective value is 3.25") { REQUIRE(bestObjectiveValue == Approx(-3.25)); }
            }
        }
    }

    GIVEN("a well defined optimization problem with only binary variables")
    {
        // min 2 - x + y s.t. x and y binary
        // => expected optimum: (x, y) = (1, 0) with best objective value 1
        Problem problem;

        // setup vars
        {
            auto x = std::make_unique<Variable>("x", Variable::Type::Binary);
            auto y = std::make_unique<Variable>("y", Variable::Type::Binary);

            problem.addVariable(std::move(x));
            problem.addVariable(std::move(y));
        }

        problem.setObjective([](const std::vector<VariableValue> &vars) {
            REQUIRE(vars.size() == 2);

            return -(2. - vars[0].value + vars[1].value);
        });

        AND_GIVEN("a solver for this problem")
        {
            Solver solver(problem);

            WHEN("solving the problem")
            {
                solver.solve();
                const auto [bestSolution, bestObjectiveValue] = solver.bestSolutionAndObjectiveValue();

                const auto bestX = bestSolution[0].value;
                const auto bestY = bestSolution[1].value;

                THEN("the best value for x is 1") { REQUIRE(bestX == Approx(1.)); }
                THEN("the best value for y is 0") { REQUIRE(bestY == Approx(0.)); }
                THEN("the best objective value is 1.") { REQUIRE(bestObjectiveValue == Approx(-1.)); }
            }
        }
    }
}

SCENARIO("a well defined problem with an inequality constraint can be solved", "[solving][constraints]")
{
    GIVEN("a well defined optimization problem with only continuous variables and an inequality constraint")
    {
        // min x^2 + y^2 s.t. 0.1 <= x <= 0.9, 1.5 <= y <= 3.8, x^2+y^2 >= 3
        Problem problem;

        // setup vars
        {
            auto x = std::make_unique<Variable>("x", Variable::Type::Continuous);
            x->setLb(0.1);
            x->setUb(0.9);

            auto y = std::make_unique<Variable>("y", Variable::Type::Continuous);
            y->setLb(1.5);
            y->setUb(3.8);

            problem.addVariable(std::move(x));
            problem.addVariable(std::move(y));
        }

        problem.setObjective([](const std::vector<VariableValue> &vars) {
            REQUIRE(vars.size() == 2);

            return -(vars[0].value * vars[0].value + vars[1].value * vars[1].value);
        });

        problem.addInequalityConstraint([](const std::vector<VariableValue> &vars) {
            return 3. - vars[0].value * vars[0].value - vars[1].value * vars[1].value;
        });

        AND_GIVEN("a solver for this problem")
        {
            Config config;

            config.setPopulationSize(200);
            config.setOutputFrequency(100);
            config.setInitialPenalizationFactor(0.01);
            config.setIncreasePenalFactorEveryNIterations(10);
            config.setAdditivePenalFactorIncrease(1.);
            config.setMultiplicativePenalFactorIncrease(1.5);

            Solver solver(problem, config);

            WHEN("solving the problem")
            {
                solver.solve();
                const auto [bestSolution, bestObjectiveValue] = solver.bestSolutionAndObjectiveValue();

                const auto bestX = bestSolution[0].value;
                const auto bestY = bestSolution[1].value;

                THEN("(best x)^2 + (best y)^2 >= 3") { REQUIRE(bestX * bestX + bestY * bestY >= Approx(3.)); }
                THEN("the best objective value is 3.") { REQUIRE(bestObjectiveValue == Approx(-3.).margin(1.E-4)); }
            }
        }
    }
}

SCENARIO("a well defined problem with an inequality constraint and an equality can be solved", "[solving][constraints]")
{
    GIVEN(
        "a well defined optimization problem with only continuous variables, an equality and an inequality constraint")
    {
        // min x^2 + y^2 s.t. 0.1 <= x <= 0.9, 1.5 <= y <= 3.8, x^2+y^2 >= 3, x + y == 2.5
        Problem problem;

        // setup vars
        {
            auto x = std::make_unique<Variable>("x", Variable::Type::Continuous);
            x->setLb(0.1);
            x->setUb(0.9);

            auto y = std::make_unique<Variable>("y", Variable::Type::Continuous);
            y->setLb(1.5);
            y->setUb(3.8);

            problem.addVariable(std::move(x));
            problem.addVariable(std::move(y));
        }

        problem.setObjective([](const std::vector<VariableValue> &vars) {
            REQUIRE(vars.size() == 2);

            return -(vars[0].value * vars[0].value + vars[1].value * vars[1].value);
        });

        problem.addInequalityConstraint([](const std::vector<VariableValue> &vars) {
            return 3. - vars[0].value * vars[0].value - vars[1].value * vars[1].value;
        });

        problem.addEqualityConstraint(
            [](const std::vector<VariableValue> &vars) { return 2.5 - vars[0].value - vars[1].value; });

        AND_GIVEN("a solver for this problem")
        {
            Config config;

            config.setPopulationSize(200);
            config.setOutputFrequency(100);
            config.setInitialPenalizationFactor(0.01);
            config.setIncreasePenalFactorEveryNIterations(10);
            config.setMultiplicativePenalFactorIncrease(10.);

            Solver solver(problem, config);

            WHEN("solving the problem")
            {
                solver.solve();
                const auto [bestSolution, bestObjectiveValue] = solver.bestSolutionAndObjectiveValue();

                const auto bestX = bestSolution[0].value;
                const auto bestY = bestSolution[1].value;

                CAPTURE(bestX, bestY);

                THEN("(best x)^2 + (best y)^2 >= 3") { REQUIRE(bestX * bestX + bestY * bestY >= Approx(3.)); }
                THEN("best x + best y == 2.5") { REQUIRE(bestX + bestY == Approx(2.5)); }
                THEN("the best objective value >= 3.") { REQUIRE(bestObjectiveValue <= Approx(-3.)); }
            }
        }
    }
}
