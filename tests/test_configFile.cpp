#include <catch2/catch.hpp>

#include <Config.h>

using namespace gao;

SCENARIO("a config object can be initialized from a config YAML file", "[config]")
{
    GIVEN("a YAML file 'gao-config.yml'")
    {
        WHEN("creating a Config object from YAML")
        {
            auto config = readConfigFromYml();

            THEN("the population size is 200") { REQUIRE(config.populationSize() == 200); }
            THEN("the mutation probability is 0.02") { REQUIRE(config.mutationProbability() == 0.02); }
            THEN("the crossover probability is 0.8") { REQUIRE(config.crossoverProbability() == 0.8); }
            THEN("the convergence tolerance is 1.E-8") { REQUIRE(config.convergenceTolerance() == 1.E-8); }
            THEN("the max. number of iterations is 20000") { REQUIRE(config.maxNumIterations() == 20000); }
            THEN("the output frequency is 100") { REQUIRE(config.outputFrequency() == 100); }
            THEN("the initial penalization factor is 0.1") REQUIRE(config.initialPenalizationFactor() == 0.1);
            THEN("the penal. factor is increase every 20 iterations")
            {
                REQUIRE(config.increasePenalFactorEveryNIterations() == 20);
            }
            THEN("the additive penal. factor increase is 0.5") { REQUIRE(config.additivePenalFactorIncrease() == 0.5); }
            THEN("the multiplicative penal. factor increase is 10")
            {
                REQUIRE(config.multiplicativePenalFactorIncrease() == 10.);
            }
        }
    }
}
