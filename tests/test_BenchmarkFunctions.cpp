#include <catch2/catch.hpp>

#include <Config.h>
#include <Problem.h>
#include <Solver.h>
#include <Variable.h>

#include <cmath>
#include <vector>

#include <fmt/core.h>

using namespace gao;

double Rastrigin(std::vector<double> x)
{
    const auto     n = x.size();
    constexpr auto A = 10;

    double sum = 0.;

    for (size_t i = 0; i < n; ++i)
    {
        sum += x[i] * x[i] - A * std::cos(2. * M_PI * x[i]);
    }

    return A * n + sum;
}

double Ackley(double x, double y)
{
    return -20. * std::exp(-0.2 * std::sqrt(0.5 * (x * x + y * y))) -
           std::exp(0.5 * (std::cos(2. * M_PI * x) + std::cos(2. * M_PI * y))) + std::exp(1) + 20.;
}

double Beale(double x, double y)
{
    return std::pow(1.5 - x + x * y, 2.) + std::pow(2.25 - x + x * y * y, 2.) + std::pow(2.625 - x + x * y * y * y, 2.);
}

double Mishra(double x, double y)
{
    return std::sin(y) * std::exp(std::pow(1. - std::cos(x), 2.)) +
           std::cos(x) * std::exp(std::pow(1. - std::sin(y), 2.)) + std::pow(x - y, 2.);
}

TEST_CASE("The solver can minimize Rastrigin's function", "[Rastrigin]")
{
    Problem problem;

    const size_t numVars = 1000;

    for (size_t i = 0; i < numVars; ++i)
    {
        auto x_i = std::make_unique<Variable>(fmt::format("x_{}", i), Variable::Type::Continuous);

        x_i->setLb(-5.12);
        x_i->setUb(5.12);

        problem.addVariable(std::move(x_i));
    }

    problem.setObjective([](const std::vector<VariableValue> &xs) {
        std::vector<double> x;

        std::transform(xs.cbegin(), xs.cend(), std::back_inserter(x), [](const VariableValue &vv) { return vv.value; });

        return -Rastrigin(x);
    });

    Config config;

    config.setPopulationSize(200);
    config.setConvergenceTolerance(1.E-14);
    config.setOutputFrequency(100);
    config.setMaxNumIterations(1E6);
    config.setMutationProbability(0.8);
    config.setCrossoverProbability(0.9);

    Solver solver(problem, config);

    solver.solve();

    const auto [bestSolution, bestObjectiveValue] = solver.bestSolutionAndObjectiveValue();

    REQUIRE(bestObjectiveValue == Approx(0.).margin(1.E-7));

    for (const auto &bestX : bestSolution)
    {
        REQUIRE(bestX.value == Approx(0.).margin(1.E-7));
    }
}

TEST_CASE("The solver can minimize Ackleys's function", "[Ackley]")
{
    Problem problem;

    {
        auto x = std::make_unique<Variable>("x", Variable::Type::Continuous);
        auto y = std::make_unique<Variable>("y", Variable::Type::Continuous);

        x->setLb(-5.);
        x->setUb(5.);

        y->setLb(-5.);
        y->setUb(5.);

        problem.addVariable(std::move(x));
        problem.addVariable(std::move(y));
    }

    problem.setObjective([](const std::vector<VariableValue> &xs) { return -Ackley(xs[0].value, xs[1].value); });

    Config config;

    config.setPopulationSize(100);
    config.setConvergenceTolerance(1.E-8);
    config.setOutputFrequency(100);
    config.setMaxNumIterations(1E6);
    config.setMutationProbability(0.1);
    config.setCrossoverProbability(0.9);

    Solver solver(problem, config);

    solver.solve();

    const auto [bestSolution, bestObjectiveValue] = solver.bestSolutionAndObjectiveValue();

    REQUIRE(bestObjectiveValue == Approx(0.).margin(1.E-7));

    for (const auto &bestX : bestSolution)
    {
        REQUIRE(bestX.value == Approx(0.).margin(1.E-7));
    }
}

TEST_CASE("The solver can minimize Beale's function", "[Beale]")
{
    Problem problem;

    {
        auto x = std::make_unique<Variable>("x", Variable::Type::Continuous);
        auto y = std::make_unique<Variable>("y", Variable::Type::Continuous);

        x->setLb(-4.5);
        x->setUb(4.5);

        y->setLb(-4.5);
        y->setUb(4.5);

        problem.addVariable(std::move(x));
        problem.addVariable(std::move(y));
    }

    problem.setObjective([](const std::vector<VariableValue> &xs) { return -Beale(xs[0].value, xs[1].value); });

    Config config;

    config.setPopulationSize(2000);
    config.setConvergenceTolerance(1.E-14);
    config.setOutputFrequency(100);
    config.setMaxNumIterations(1E6);
    config.setMutationProbability(0.7);
    config.setCrossoverProbability(0.9);

    Solver solver(problem, config);

    solver.solve();

    const auto [bestSolution, bestObjectiveValue] = solver.bestSolutionAndObjectiveValue();

    // Beale's function is quite flat around the optimum, so the tolerances are quite high here
    REQUIRE(bestObjectiveValue == Approx(0.).margin(1.E-2));

    const auto bestX = bestSolution[0].value;
    const auto bestY = bestSolution[1].value;

    REQUIRE(bestX == Approx(3.).margin(1.E-1));
    REQUIRE(bestY == Approx(0.5).margin(1.E-1));
}

TEST_CASE("The solver can minimize constrained Mishra's Bird function", "[Mishra]")
{
    Problem problem;

    {
        auto x = std::make_unique<Variable>("x", Variable::Type::Continuous);
        auto y = std::make_unique<Variable>("y", Variable::Type::Continuous);

        x->setLb(-10.);
        x->setUb(0.);

        y->setLb(-6.5);
        y->setUb(0.);

        problem.addVariable(std::move(x));
        problem.addVariable(std::move(y));
    }

    problem.setObjective([](const std::vector<VariableValue> &xs) { return -Mishra(xs[0].value, xs[1].value); });

    problem.addInequalityConstraint([](const std::vector<VariableValue> &xs) {
        constexpr double epsilon = 1.E-10;
        return std::pow(xs[0].value + 5, 2) + std::pow(xs[1].value + 5, 2) - 25 - epsilon;
    });

    Config config;

    config.setPopulationSize(1500);
    config.setConvergenceTolerance(1.E-14);
    config.setOutputFrequency(100);
    config.setMaxNumIterations(1E6);
    config.setMutationProbability(0.8);
    config.setCrossoverProbability(0.9);
    config.setMultiplicativePenalFactorIncrease(100.);
    config.setIncreasePenalFactorEveryNIterations(5);

    Solver solver(problem, config);

    solver.solve();

    const auto [bestSolution, bestObjectiveValue] = solver.bestSolutionAndObjectiveValue();

    REQUIRE(bestObjectiveValue == Approx(106.7645367).margin(1.E-2));

    const auto bestX = bestSolution[0].value;
    const auto bestY = bestSolution[1].value;

    REQUIRE(bestX == Approx(-3.1302468).margin(1.E-2));
    REQUIRE(bestY == Approx(-1.5821422).margin(1.E-2));
}
