#include <catch2/catch.hpp>

#include <Variable.h>

#include <limits>

using namespace gao;

SCENARIO("scalar variables can be created for values of types double, int, and bool, and they have the expeted default "
         "lower and upper bounds",
         "[variables]")
{
    WHEN("creating a continuous variable")
    {
        auto var = Variable("x", Variable::Type::Continuous);

        THEN("its name is 'x'") { REQUIRE(var.name() == "x"); }

        THEN("its type is Continuous") { REQUIRE(var.type() == Variable::Type::Continuous); }

        THEN("its lower bound equals 0.49*std::numeric_limits<double>::lowest()")
        {
            REQUIRE(var.lb() == 0.49 * std::numeric_limits<double>::lowest());
        }

        THEN("its upper bound equals 0.49*std::numeric_limits<double>::max()")
        {
            REQUIRE(var.ub() == 0.49 * std::numeric_limits<double>::max());
        }
    }

    WHEN("creating an integer variable")
    {
        auto var = Variable("x", Variable::Type::Integer);

        THEN("its name is 'x'") { REQUIRE(var.name() == "x"); }

        THEN("its type is Integer") { REQUIRE(var.type() == Variable::Type::Integer); }

        THEN("its lower bound equals std::numeric_limits<int>::lowest()")
        {
            REQUIRE(var.lb() == std::numeric_limits<int>::lowest());
        }

        THEN("its upper bound equals std::numeric_limits<int>::max()")
        {
            REQUIRE(var.ub() == std::numeric_limits<int>::max());
        }
    }

    WHEN("creating a binary variable")
    {
        auto var = Variable("x", Variable::Type::Binary);

        THEN("its name is 'x'") { REQUIRE(var.name() == "x"); }

        THEN("its type is Binary") { REQUIRE(var.type() == Variable::Type::Binary); }

        THEN("its lower bound equals 0") { REQUIRE(var.lb() == 0); }

        THEN("its upper bound equals 1") { REQUIRE(var.ub() == 1); }

        AND_WHEN("setting its lower bound to something")
        {
            var.setLb(1.7);

            THEN("it has no effect and its lower bound is still 0") { REQUIRE(var.lb() == 0.); }
        }

        AND_WHEN("setting its upper bound to something")
        {
            var.setUb(1.7);

            THEN("it has no effect and its lower bound is still 1") { REQUIRE(var.ub() == 1.); }
        }
    }
}
