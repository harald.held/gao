#include <gao/Problem.h>
#include <gao/Variable.h>

int main()
{
    gao::Problem problem;

    // setup vars
    {
        auto x = std::make_unique<gao::Variable>("x", gao::Variable::Type::Continuous);
        x->setLb(0.1);
        x->setUb(0.9);

        auto y = std::make_unique<gao::Variable>("y", gao::Variable::Type::Continuous);
        y->setLb(1.5);
        y->setUb(3.8);

        problem.addVariable(std::move(x));
        problem.addVariable(std::move(y));
    }

    problem.setObjective([](const std::vector<gao::VariableValue> &vars) {
        return -(vars[0].value * vars[0].value + vars[1].value * vars[1].value);
    });
}
