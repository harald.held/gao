# gao
`gao` is a small experiment to play with genetic algorithms to solve MINLPs (Mixed-Integer NonLinear Programs). Examples how to use it can be found amongst the unit tests.

Note that since the algorithms relies a lot on random numbers, unit tests failing occasionally can be expected. However, they should pass more often than not ...


# Build
You need [CMake](https://cmake.org/) and [Conan](https://conan.io/), and of course a suitable C++ compiler (this was only tested on Linux with gcc 9).

### Create Conan package
```
conan create . -s build_type=Debug --build=missing
conan create . -s build_type=Release --build=missing
```
